<?php

declare(strict_types=1);
namespace Drupal\confirmation\Entity;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the confirmation entity type.
 */
class ConfirmationListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new ConfirmationListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total confirmations: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['state'] = $this->t('State');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    $header['last_sent'] = $this->t('Last sent');
    $header['expiration'] = $this->t('Expiration');
    $header['email'] = $this->t('E-Mail');
    $header['response_link'] = $this->t('Response link');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\confirmation\Entity\ConfirmationInterface $entity */
    $row['id'] = $entity->toLink();
    $row['state'] = $this->formatState($entity->get('state')->value);
    $row['created'] = $this->dateFormatter->format($entity->get('created')->value);
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime());
    $row['last_sent'] = $entity->get('last_sent')->value ?
      $this->dateFormatter->format($entity->get('last_sent')->value) : '';
    $row['expiration'] = $this->dateFormatter->format($entity->get('expiration')->value);
    $row['email'] = $entity->get('email')->value;
    $row['response_link'] = Link::fromTextAndUrl($this->t('Respond'), $entity->getResponseUrl())->toString();
    return $row + parent::buildRow($entity);
  }

  /**
   * @param mixed $state
   *   The state from config. Core does NOT always return a bool here.
   */
  protected function formatState($state): TranslatableMarkup {
    if (is_string($state)) {
      $state = intval($state);
    }
    if (isset($state)) {
      if ($state) {
        return $this->t('Accepted');
      }
      else {
        return $this->t('Rejected');
      }
    }
    else {
      return t('Undecided');
    }
  }

}
