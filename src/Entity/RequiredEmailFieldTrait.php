<?php

declare(strict_types=1);
namespace Drupal\confirmation\Entity;

use Drupal\Core\Field\BaseFieldDefinition;

trait RequiredEmailFieldTrait {

  /**
   * @param string $field_name
   * @return \Drupal\Core\Field\FieldItemListInterface
   */
  abstract protected function get($field_name);

  /**
   * @param string $field_name
   * @param mixed $value
   * @param bool $notify
   * @return $this
   */
  abstract protected function set($name, $value, $notify = TRUE);

  private static function makeEmailFieldRequired(array &$fields, array $base_field_definitions): BaseFieldDefinition {
    /** @var \Drupal\Core\Field\BaseFieldDefinition $emailBaseField */
    $emailBaseField = $base_field_definitions['email'];
    $fields['email'] = $emailBaseField
      ->setRequired(TRUE);
    return $emailBaseField;
  }

  public function getEmail(): string {
    return $this->get('email')->getString();
  }

  /**
   * @return $this
   */
  public function setEmail(string $email) {
    $this->set('email', $email);
    return $this;
  }

}
