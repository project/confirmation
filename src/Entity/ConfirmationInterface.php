<?php

namespace Drupal\confirmation\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Url;

/**
 * Provides an interface defining a confirmation entity type.
 */
interface ConfirmationInterface extends ContentEntityInterface, EntityChangedInterface {

  public function getHash(): string;

  /**
   * @return $this
   */
  public function setExpirationInterval(int $timestamp);

  public function getExpirationTime(): int;

  public function isExpired(): bool;

  public function getCreatedTime(): int;

  public function getLastSentTime(): ?int;

  /**
   * @return $this
   */
  public function setLastSentTime(int $timestamp = NULL);

  public function getState(): ?bool;

  public function setState(?bool $state): void;

  /**
   * @deprecated in confirmation 1.0.1, will be removed before 2.0.0.
   *    Use ::setState().
   *
   * @return $this
   */
  public function setStateConfirmed();

  /**
   * @deprecated in confirmation 1.0.1, will be removed before 2.0.0.
   *     Use ::setState().
   *
   * @return $this
   */
  public function setStateDisconfirmed();

  /**
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getConfirmationQuestion();

  public function getResponseUrl(): Url;

  public static function getEntityIdsToPurge(string $bundle, int $limit): array;

}
