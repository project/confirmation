<?php

declare(strict_types=1);
namespace Drupal\confirmation\Entity;

use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Defines the confirmation entity class.
 *
 * @ContentEntityType(
 *   id = "confirmation",
 *   bundle_plugin_type = "confirmation",
 *   label = @Translation("Confirmation"),
 *   label_collection = @Translation("Confirmations"),
 *   label_singular = @Translation("confirmation"),
 *   label_plural = @Translation("confirmations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count confirmations",
 *     plural = "@count confirmations",
 *   ),
 *   handlers = {
 *     "bundle_plugin" =
 *   "Drupal\confirmation\BundlePlugin\BundleClassPluginHandler",
 *     "list_builder" = "Drupal\confirmation\Entity\ConfirmationListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\confirmation\Entity\Form\ConfirmationEditForm",
 *       "edit" = "Drupal\confirmation\Entity\Form\ConfirmationEditForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "response" =
 *   "Drupal\confirmation\Entity\Form\ConfirmationResponseForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "confirmation",
 *   admin_permission = "administer confirmation",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "bundle" = "bundle",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/confirmations",
 *     "add-page" = "/admin/content/confirmations/confirmation/add",
 *     "add-form" = "/admin/content/confirmations/confirmation/add/{bundle}",
 *     "canonical" =
 *   "/admin/content/confirmations/confirmation/{confirmation}",
 *     "edit-form" =
 *   "/admin/content/confirmations/confirmation/{confirmation}/edit",
 *     "delete-form" =
 *   "/admin/content/confirmations/confirmation/{confirmation}/delete",
 *     "response-form" = "/confirmation/{confirmation}/{hash}",
 *   },
 *   field_ui_base_route = "entity.confirmation.bundle",
 * )
 */
abstract class Confirmation extends ContentEntityBase implements ConfirmationInterface {

  use EntityChangedTrait;

  private ?bool $originalState;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['state'] = BaseFieldDefinition::create('boolean')
      ->setRequired(FALSE)
      ->setLabel(t('State'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => '0',
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'weight' => '0',
        'label' => 'inline',
        'settings' => [
          'format' => 'default',
        ],
      ])
      ->setSetting('on_label', new TranslatableMarkup('Accepted'))
      ->setSetting('off_label', new TranslatableMarkup('Rejected'))
    ;

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setRequired(TRUE)
      ->setLabel(t('Created on'))
      // Form must not change this.
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'timestamp',
        'weight' => '2',
        'label' => 'inline',
        'settings' => [
          'date_format' => 'custom',
          'custom_date_format' => 'Y-m-d H:i:s',
          'timezone' => '',
        ],
      ])
    ;

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setRequired(TRUE)
      ->setLabel(t('Changed'))
      // Form must not change this.
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'timestamp',
        'weight' => '2',
        'label' => 'inline',
        'settings' => [
          'date_format' => 'custom',
          'custom_date_format' => 'Y-m-d H:i:s',
          'timezone' => '',
        ],
      ])
    ;

    $fields['hash'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Hash'))
      ->setDefaultValueCallback(static::class . '::createHash')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['last_sent'] = BaseFieldDefinition::create('timestamp')
      ->setRequired(FALSE)
      ->setLabel(t('Last sent'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => '1',
      ])
      ->setDisplayOptions('view', [
        'type' => 'timestamp',
        'weight' => '1',
        'label' => 'inline',
        'settings' => [
          'date_format' => 'custom',
          'custom_date_format' => 'Y-m-d H:i:s',
          'timezone' => '',
        ],
      ])
    ;

    $fields['expiration'] = BaseFieldDefinition::create('timestamp')
      ->setRequired(TRUE)
      ->setLabel(t('Expiration'))
      ->setDefaultValueCallback(static::class . '::createExpiration')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => '2',
      ])
      ->setDisplayOptions('view', [
        'type' => 'timestamp',
        'weight' => '2',
        'label' => 'inline',
        'settings' => [
          'date_format' => 'custom',
          'custom_date_format' => 'Y-m-d H:i:s',
          'timezone' => '',
        ],
      ])
    ;

    $fields['email'] = BaseFieldDefinition::create('email')
      // Bundles can make this required and add getters / setters.
      ->setRequired(FALSE)
      ->setLabel(t('Email'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => '3',
        'settings' => [
          'placeholder' => '',
          'size' => '60',
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'basic_string',
        'weight' => '3',
        'label' => 'inline',
      ])
    ;

    return $fields;
  }

  protected function urlRouteParameters($rel) {
    $routeParameters = parent::urlRouteParameters($rel);
    if ($rel === 'response-form') {
      $routeParameters[$this->getEntityTypeId()] = $this->id();
      $routeParameters['hash'] = $this->getHash();
    }
    return $routeParameters;
  }


  public static function createHash(): string {
    return bin2hex(random_bytes(20));
  }

  public function getHash(): string {
    return $this->get('hash')->getString();
  }

  public static function createExpiration(): int {
    return \Drupal::time()->getRequestTime() + static::getExpirationInterval();
  }

  public static function getExpirationInterval(): int {
    return 7 * 86400;
  }

  public function setExpirationInterval(int $interval) {
    $this->set('expiration', $interval + \Drupal::time()->getRequestTime());
    return $this;
  }

  public function getExpirationTime(): int {
    return intval($this->get('expiration')->getString());
  }

  public function isExpired(): bool {
    return $this->getExpirationTime() <= \Drupal::time()->getRequestTime();
  }

  public function getLastSentTime(): ?int {
    $itemList = $this->get('last_sent');
    return !$itemList->isEmpty() ? intval($itemList->getString()) : NULL;
  }

  public function setLastSentTime(int $timestamp = NULL) {
    $timestamp = $timestamp ?? \Drupal::time()->getRequestTime();
    $this->set('last_sent', $timestamp);
    return $this;
  }

  public function getCreatedTime(): int {
    return intval($this->get('created')->getString());
  }

  public static function getEntityIdsToPurge(string $bundle, int $limit): array {
    $now = \Drupal::time()->getRequestTime();
    $query = \Drupal::entityTypeManager()->getStorage('confirmation')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('bundle', $bundle)
      ->condition('expiration', $now, '<=')
      ->range(0, $limit);
    return $query->execute();
  }

  public function getState(): ?bool {
    $item = $this->get('state')->first();
    return $item ? boolval($item->get('value')->getValue()) : NULL;
  }

  public function setState(?bool $state): void {
    $this->set('state', $state);
  }

  public function setStateConfirmed() {
    @trigger_error('Confirmation::setStateConfirmed was deprecated in confirmation 1.0.1, will be removed before 2.0.0.', E_USER_DEPRECATED);
    $this->set('state', TRUE);
    return $this;
  }

  public function setStateDisconfirmed() {
    @trigger_error('Confirmation::setStateDisconfirmed was deprecated in confirmation 1.0.1, will be removed before 2.0.0.', E_USER_DEPRECATED);
    $this->set('state', FALSE);
    return $this;
  }

  public function getResponseUrl(): Url {
    return $this->toUrl('response-form', ['absolute' => TRUE]);
  }

  /**
   * Allows bundle classes to react on confirmed / disconfirmed state.
   *
   * Implementing classes do not need to call parent.
   */
  protected function hookConfirmationStateSettled(bool $state): void {}

  private function checkConfirmationStateSettled(): void {
    $state = $this->getState();
    if (!isset($this->originalState) && isset($state)) {
      $this->hookConfirmationStateSettled($state);
    }
  }

  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $this->checkConfirmationStateSettled();
  }

  private function snapshotOriginalState(): void {
    $this->originalState = $this->getState();
  }

  public function postCreate(EntityStorageInterface $storage) {
    parent::postCreate($storage);
    $this->snapshotOriginalState();
  }

  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    parent::postLoad($storage, $entities);
    /** @var static $entity */
    foreach ($entities as $entity) {
      $entity->snapshotOriginalState();
    }
  }

}
