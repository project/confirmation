<?php

namespace Drupal\confirmation\Entity;

interface ConfirmationWithEmailInterface {

  public function getEmail(): string;

  /**
   * @return $this
   */
  public function setEmail(string $email);


}
