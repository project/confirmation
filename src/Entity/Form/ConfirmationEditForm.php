<?php

declare(strict_types=1);
namespace Drupal\confirmation\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the confirmation entity edit forms.
 */
class ConfirmationEditForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New confirmation %label has been created.', $message_arguments));
        $this->logger('confirmation')->notice('Created new confirmation %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The confirmation %label has been updated.', $message_arguments));
        $this->logger('confirmation')->notice('Updated confirmation %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.confirmation.canonical', ['confirmation' => $entity->id()]);

    return $result;
  }

}
