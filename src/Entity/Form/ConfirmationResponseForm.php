<?php

declare(strict_types=1);
namespace Drupal\confirmation\Entity\Form;

use Drupal\confirmation\Utility\ThrowMethodTrait;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Ask for confirmation / disconfirmation.
 *
 * @todo Find a way to make this form pluggable per bundle.
 */
final class ConfirmationResponseForm extends EntityConfirmFormBase {

  use ThrowMethodTrait;

  public function getBaseFormId() {
    return $this->entity->getEntityTypeId() . '_response_form';
  }

  public function getCancelUrl() {
    return ''; // Not needed.
  }

  public function getQuestion() {
    return $this->entity->getConfirmationQuestion();
  }

  public function getConfirmText() {
    return $this->t('Submit');
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\confirmation\Entity\ConfirmationInterface $confirmation */
    $confirmation = $this->getEntity();
    $state = $confirmation->getState();
    if ($state === TRUE) {
      $form['#title'] = $this->t('This item has been confirmed.');
    }
    elseif ($state === FALSE) {
      $form['#title'] = $this->t('This item has been disconfirmed.');
    }
    elseif ($confirmation->isExpired()) {
      $form['#title'] = $this->t('This item has expired.');
    }
    else {
      $this->buildResponseForm($form, $form_state);
    }

    return $form;
  }

  public function buildResponseForm(array &$form, FormStateInterface $form_state): void {
    // Build the response form.
    $form = parent::buildForm($form, $form_state);
    unset($form['#process']);
    unset($form['#after_build']);
    // No cancel option needed.
    unset($form['actions']['cancel']);

    $form['state'] = [
      '#type' => 'radios',
      '#default_value' => 'confirm',
      '#options' => [
        'confirm' => $this->t('Confirm'),
        'disconfirm' => $this->t('Disconfirm'),
      ],
    ];
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\confirmation\Entity\ConfirmationInterface $confirmation */
    $confirmation = $this->getEntity();
    $confirmKey = $form_state->getValue('state') ?? self::throwUnexpectedValue();
    if ($confirmKey === 'confirm') {
      $confirmation->setState(TRUE);
      $this->messenger()->addStatus($this->t('The action was confirmed.'));
    }
    elseif ($confirmKey === 'disconfirm') {
      $confirmation->setState(FALSE);
      $this->messenger()->addStatus($this->t('The action was disconfirmed.'));
    }
    else {
      throw new \UnexpectedValueException();
    }
    $confirmation->save();
    // @todo Implement redirect after submit.
  }

}
