<?php

declare(strict_types=1);
namespace Drupal\confirmation\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines confirmation annotation object.
 *
 * @Annotation
 */
class ConfirmationBundle extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
