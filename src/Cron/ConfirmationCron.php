<?php

declare(strict_types=1);
namespace Drupal\confirmation\Cron;

use Drupal\confirmation\Entity\Confirmation;
use Drupal\confirmation\Utility\ThrowMethodTrait;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;

final class ConfirmationCron implements ConfirmationCronInterface {

  use ThrowMethodTrait;

  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  protected int $limit;

  public function __construct(EntityTypeBundleInfoInterface $entityTypeBundleInfo, int $limit) {
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->limit = $limit;
  }

  public function hookCron(): void {
    $itemsToProcess = $this->limit;
    foreach ($this->entityTypeBundleInfo->getBundleInfo('confirmation') as $bundle => $bundleInfo) {
      // @see \Drupal\Core\Entity\ContentEntityStorageBase::getEntityClass
      /** @var \Drupal\confirmation\Entity\Confirmation $bundleClass */
      $bundleClass = $bundleInfo['class'] ?? self::throwLogic();
      $ids = $bundleClass::getEntityIdsToPurge($bundle, $itemsToProcess);
      foreach ($ids as $id) {
        Confirmation::load($id)->delete();
      }
      $itemsToProcess -= count($ids);
      if ($itemsToProcess <= 0) {
        break;
      }
    }
  }

}
