<?php

namespace Drupal\confirmation\Cron;

interface ConfirmationCronInterface {

  public function hookCron(): void;

}
