<?php

declare(strict_types=1);
namespace Drupal\confirmation\BundlePlugin;

interface ConfirmationBundleClassManagerInterface {

  public function getBundleClasses(): array;

  public function getBundleClass(string $pluginId): string;

}
