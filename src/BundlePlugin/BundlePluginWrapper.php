<?php

declare(strict_types=1);
namespace Drupal\confirmation\BundlePlugin;

use Drupal\confirmation\Utility\ThrowMethodTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\BundlePlugin\BundlePluginInterface;

final class BundlePluginWrapper implements BundlePluginInterface {

  use ThrowMethodTrait;

  protected EntityTypeInterface $entityType;

  protected string $pluginId;

  protected array $pluginDefinition;

  protected string $bundleClass;

  public function __construct(EntityTypeInterface $entityType, string $pluginId, array $pluginDefinition, string $bundleClass) {
    $this->entityType = $entityType;
    $this->pluginId = $pluginId;
    $this->pluginDefinition = $pluginDefinition;
    $this->bundleClass = $bundleClass;
    if (!is_subclass_of($this->bundleClass, ContentEntityInterface::class)) {
      throw new \UnexpectedValueException();
    }
  }

  public static function create(array $pluginDefinition, EntityTypeInterface $entityType): self {
    return new static(
      $entityType,
      $pluginDefinition['id'] ?? self::throwUnexpectedValue(),
      $pluginDefinition,
      $pluginDefinition['class'] ?? self::throwUnexpectedValue(),
    );
  }

  public function getPluginId() {
    return $this->pluginId;
  }

  public function getPluginDefinition() {
    return $this->pluginDefinition;
  }

  /**
   * Get bundle field definitions from bundle class.
   *
   * Levarage ::bundleFieldDefinitions(), the bundle class implementation of
   * which currently is not used by core.
   *
   * @see \Drupal\Core\Entity\EntityFieldManager::buildBundleFieldDefinitions
   */
  public function buildFieldDefinitions() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $bundleClass */
    $bundleClass = $this->bundleClass;
    $baseFieldDefinitions = $bundleClass::baseFieldDefinitions($this->entityType);
    return $bundleClass::bundleFieldDefinitions($this->entityType, $this->pluginId, $baseFieldDefinitions);
  }

}
