<?php

declare(strict_types=1);
namespace Drupal\confirmation\BundlePlugin;

use Drupal\entity\BundlePlugin\BundlePluginHandler;

final class BundleClassPluginHandler extends BundlePluginHandler {

  public function getBundleInfo() {
    $bundles = parent::getBundleInfo();
    foreach ($this->pluginManager->getDefinitions() as $pluginId => $definition) {
      $bundles[$pluginId]['class'] = $definition['class'];
    }
    return $bundles;
  }

}
