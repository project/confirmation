<?php

declare(strict_types=1);
namespace Drupal\confirmation\BundlePlugin;

use Drupal\confirmation\Annotation\ConfirmationBundle;
use Drupal\confirmation\Entity\ConfirmationInterface;
use Drupal\confirmation\Utility\ThrowMethodTrait;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Confirmation plugin manager.
 */
class ConfirmationBundleClassManager extends DefaultPluginManager implements ConfirmationBundleClassManagerInterface{

  use ThrowMethodTrait;

  const CONFIRMATION_BUNDLE_LIST_TAG = 'confirmation_bundle_list';

  /**
   * Constructs ConfirmationPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ConfirmationBundle',
      $namespaces,
      $module_handler,
      ConfirmationInterface::class,
      ConfirmationBundle::class
    );
    $this->alterInfo('confirmation_info');
    $this->setCacheBackend($cache_backend, 'confirmation_plugins', [self::CONFIRMATION_BUNDLE_LIST_TAG]);
  }

  /**
   * Create an instance implementing BundlePluginInterface.
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $entityType = \Drupal::entityTypeManager()->getDefinition('confirmation');
    return BundlePluginWrapper::create($this->getDefinition($plugin_id), $entityType);
  }

  public function getBundleClasses(): array {
    return array_map(
      fn(array $definition) => $definition['class'],
      $this->getDefinitions()
    );
  }

  public function getBundleClass(string $pluginId): string {
    return $this->getBundleClasses()[$pluginId] ?? self::throwLogic($pluginId);
  }

}
