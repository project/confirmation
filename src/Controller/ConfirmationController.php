<?php

declare(strict_types=1);
namespace Drupal\confirmation\Controller;

use Drupal\confirmation\BundlePlugin\ConfirmationBundleClassManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

final class ConfirmationController implements ContainerInjectionInterface {

  protected EntityTypeManagerInterface $entityTypeManager;

  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  private function __construct(EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  public function getEntityTypeId(): string {
    return 'confirmation';
  }

  public function listBundles() {
    $entityTypeId = $this->getEntityTypeId();

    $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($entityTypeId);
    $bundles = array_combine(array_keys($bundleInfo), array_keys($bundleInfo));
    $bundleLabels = array_map(fn($info) => $info['label'], $bundleInfo);
    $bundleUrls = array_map(
      fn(string $bundle) => Url::fromRoute("entity.$entityTypeId.field_ui_fields", ['bundle' => $bundle]),
      $bundles
    );
    $bundleLinks = array_map(
      fn(string $bundle) => Link::fromTextAndUrl($bundleLabels[$bundle], $bundleUrls[$bundle]),
      $bundles
    );

    $build = [
      '#theme' => 'item_list',
      '#items' => $bundleLinks,
      '#cache' => [
        // @todo Add cacheability on add page too.
        'tags' => [ConfirmationBundleClassManager::CONFIRMATION_BUNDLE_LIST_TAG],
      ],
    ];

    return $build;
  }

  public function showBundle() {
    // This empty route is needed by field UI.
    return [];
  }

}
