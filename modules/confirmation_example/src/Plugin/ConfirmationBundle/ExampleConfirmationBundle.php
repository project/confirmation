<?php

declare(strict_types=1);
namespace Drupal\confirmation_example\Plugin\ConfirmationBundle;

use Drupal\confirmation\Annotation\ConfirmationBundle;
use Drupal\confirmation\Entity\Confirmation;
use Drupal\confirmation\Entity\ConfirmationWithEmailInterface;
use Drupal\confirmation\Entity\RequiredEmailFieldTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;

/**
 * Plugin implementation of the confirmation.
 *
 * @ConfirmationBundle(
 *   id = "confirmation_example",
 *   label = @Translation("Confirmation example"),
 *   description = @Translation("Shows how a confirmation bundle is implemented and serves for tests.")
 * )
 */
class ExampleConfirmationBundle extends Confirmation implements ConfirmationWithEmailInterface {

  use StringTranslationTrait;
  use RequiredEmailFieldTrait;

  public static function createInstance() {
    return static::create(['bundle' => 'confirmation_example']);
  }

  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields['confirmation_example__node'] = BaseFieldDefinition::create('entity_reference')
      ->setRequired(TRUE)
      ->setSetting('target_type', 'node')
      ->setLabel(t('Referenced Node'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    self::makeEmailFieldRequired($fields, $base_field_definitions);

    return $fields;
  }

  public function getNode(): NodeInterface {
    return $this->get('confirmation_example__node')->first()->get('entity')->getValue();
  }

  /**
   * @return $this
   */
  public function setNode(NodeInterface $node) {
    $this->set('confirmation_example__node', $node);
    return $this;
  }

  protected function hookConfirmationStateSettled(bool $state): void {
    if ($state) {
      $this->getNode()->setPublished()->save();
    }
    else {
      $this->getNode()->delete();
    }
  }

  public function getConfirmationQuestion() {
    return $this->t('Confirm publishing of node: %label', ['%label' => $this->getNode()->label()]);
  }

}
