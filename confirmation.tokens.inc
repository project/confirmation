<?php

use Drupal\confirmation\Entity\ConfirmationInterface;
use Drupal\confirmation\Entity\ConfirmationWithEmailInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function confirmation_token_info() {
  $types['confirmation'] = [
    'name' => t('Confirmation'),
    'description' => t('Tokens related to confirmations.'),
    'needs-data' => 'confirmation',
  ];

  $tokens['response-url'] = [
    'name' => t('Confirmation URL'),
    'description' => t('The URL of the page where users can confirm their email address.'),
    'type' => 'url',
  ];

  $tokens['email'] = [
    'name' => t('Confirmation email'),
    'description' => t('The email address to confirm.'),
  ];

  $tokens['expiration'] = [
    'name' => t('Expiration time'),
    'description' => t('The time the confirmation expires.'),
    'type' => 'date',
  ];

  return [
    'types' => $types,
    'tokens' => [
      'confirmation' => $tokens,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function confirmation_tokens($type, $tokens, $data, $options, BubbleableMetadata $bubbleable_metadata) {
  $urlOptions = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $urlOptions['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = LanguageInterface::LANGCODE_DEFAULT;
  }

  $replacements = [];
  if ($type === 'confirmation') {
    $confirmation = $data['confirmation'] ?? NULL;
    if ($confirmation  instanceof ConfirmationInterface) {
      foreach ($tokens as $key => $token) {
        switch ($key) {
          case 'response-url':
            $url = $confirmation->getResponseUrl();
            $url->setOptions($urlOptions);
            $replacements[$token] = $url->toString();
            $bubbleable_metadata->addCacheableDependency($confirmation);
            break;
          case 'expiration':
            $timeStamp = $confirmation->getExpirationTime();
            $replacements[$token] = $timeStamp;
            $bubbleable_metadata->addCacheableDependency($confirmation);
            break;
          case 'email':
            if ($confirmation instanceof ConfirmationWithEmailInterface) {
              $replacements[$token] = $confirmation->getEmail();
              $bubbleable_metadata->addCacheableDependency($confirmation);
            }
            break;
        }
      }
    }

    if ($chainTokens = \Drupal::token()->findWithPrefix($tokens, 'expiration')) {
      $replacements += \Drupal::token()->generate('date', $chainTokens, ['date' => $confirmation->getExpirationTime()], $options, $bubbleable_metadata);
    }

    if ($chainTokens = \Drupal::token()->findWithPrefix($tokens, 'response-url')) {
      $replacements += \Drupal::token()->generate('url', $chainTokens, ['url' => $confirmation->getResponseUrl()], $options, $bubbleable_metadata);
    }

  }

  return $replacements;
}
