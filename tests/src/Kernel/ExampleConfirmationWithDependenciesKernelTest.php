<?php

declare(strict_types=1);

namespace Drupal\Tests\confirmation\Kernel;

use Drupal\confirmation_example\Plugin\ConfirmationBundle\ExampleConfirmationBundle;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Kernel tests with more dependencies.
 *
 * @group confirmation
 */
final class ExampleConfirmationWithDependenciesKernelTest extends KernelTestBase {

  protected NodeInterface $node;

  protected function setUp(): void {
    parent::setUp();
    $this->enableModules([
      'confirmation_example',
      'confirmation',
      'entity',
      'node',
      'user',
      'field',
    ]);
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('confirmation');

    $this->node = Node::create([
      'type' => 'article',
      'title' => 'ArticleToConfirm',
    ]);
    $this->node->save();
  }

  public function testDefaultExpiration() {
    $future1 = ExampleConfirmationBundle::create(['bundle' => 'confirmation_example',])
      ->setNode($this->node);
    $this->assertSame(ExampleConfirmationBundle::getExpirationInterval(), $future1->getExpirationTime() - \Drupal::time()->getRequestTime());
  }

  public function testPurge() {
    $future1 = ExampleConfirmationBundle::create(['bundle' => 'confirmation_example',])
      ->setNode($this->node);
    $future1->save();

    $future2 = ExampleConfirmationBundle::create(['bundle' => 'confirmation_example',])
      ->setNode($this->node);
    $future2->save();

    $past1 = ExampleConfirmationBundle::create(['bundle' => 'confirmation_example',])
      ->setNode($this->node)
      ->setExpirationInterval(-1);
    $past1->save();

    $this->assertSame(TRUE, $past1->isExpired());
    $this->assertSame(FALSE, $future1->isExpired());

    $toPurge = ExampleConfirmationBundle::getEntityIdsToPurge('confirmation_example', 100);
    self::assertSame([$past1->id()], array_values($toPurge));
  }

}
