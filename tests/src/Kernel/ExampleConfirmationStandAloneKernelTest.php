<?php

declare(strict_types=1);

namespace Drupal\Tests\confirmation\Kernel;

use Drupal\confirmation_example\Plugin\ConfirmationBundle\ExampleConfirmationBundle;
use Drupal\KernelTests\KernelTestBase;

/**
 * Standalone kernel tests.
 *
 * @group confirmation
 */
final class ExampleConfirmationStandAloneKernelTest extends KernelTestBase {

  protected function setUp(): void {
    parent::setUp();
    $this->enableModules([
      'confirmation_example',
      'confirmation',
      'entity',
      'node',
      'field',
    ]);
    $this->installEntitySchema('confirmation');
  }

  public function testConfirmationBundles() {
    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo */
    $entityTypeBundleInfo = \Drupal::service('entity_type.bundle.info');
    $bundleInfo = $entityTypeBundleInfo->getBundleInfo('confirmation');
    $this->assertSame(['confirmation_example'], array_keys($bundleInfo));
    $this->assertSame(ExampleConfirmationBundle::class, $bundleInfo['confirmation_example']['class'] ?? NULL);
  }

  public function testConfirmationTokens() {
    \Drupal::moduleHandler()->loadInclude('confirmation', 'inc', 'confirmation.tokens');

    $confirmation = ExampleConfirmationBundle::createInstance()
      ->setEmail('noam.chomski@example.com');
    $confirmation->save();
    $template = 'Confirm [confirmation:email] at [confirmation:response-url] until [confirmation:expiration]';
    $result = \Drupal::token()->replacePlain($template, ['confirmation' => $confirmation]);
    // @see \PHPUnit\Framework\Constraint\StringMatchesFormatDescription::createPatternFromFormat
    $this->assertStringMatchesFormat('Confirm noam.chomski@example.com at http://%s/confirmation/1/%x until %s', $result);
  }

}
