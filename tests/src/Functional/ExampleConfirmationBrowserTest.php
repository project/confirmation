<?php

namespace Drupal\Tests\confirmation\Functional;

use Drupal\confirmation_example\Plugin\ConfirmationBundle\ExampleConfirmationBundle;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple browser test.
 *
 * @group confirmation
 */
class ExampleConfirmationBrowserTest extends BrowserTestBase {

  protected static $modules = [
    'confirmation_example',
  ];

  protected $defaultTheme = 'stark';

  protected function reLoadEntity(?EntityInterface &$entity): void {
    // MUST use ::loadUnchanged, as entity update on SUT does not invalidate static cache.
    $entity = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId())->loadUnchanged($entity->id());
  }

  protected function setUp(): void {
    parent::setUp();
    NodeType::create([
      'type' => 'article',
      'name' => 'Article',
    ]);
  }

  /**
   * @dataProvider provideConfirmationData
   */
  public function testConfirmation(bool $doConfirm) {
    $node = Node::create([
      'type' => 'article',
      'title' => 'ArticleToConfirm',
      'status' => FALSE,
    ]);
    $node->save();

    $this->assertSame(FALSE, $node->isPublished());

    $confirmation = ExampleConfirmationBundle::create(['bundle' => 'confirmation_example',])
      ->setNode($node)
      ->setLastSentTime();
    $confirmation->save();

    $this->drupalGet($confirmation->getResponseUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([
      'state' => $doConfirm ? 'confirm' : 'disconfirm',
    ], t('Submit'));
    $this->assertSession()->statusCodeEquals(200);

    if ($doConfirm) {
      $this->assertSession()->pageTextContains('The action was confirmed.');
    }
    else {
      $this->assertSession()->pageTextContains('The action was disconfirmed.');
    }

    $this->reLoadEntity($confirmation);
    $this->assertSame($doConfirm, $confirmation->getState());

    $this->reLoadEntity($node);
    if ($doConfirm) {
      $this->assertSame(TRUE, $node->isPublished());
    }
    else {
      $this->assertEmpty($node);
    }
  }

  public function provideConfirmationData() {
    return [[FALSE], [TRUE]];
  }

}
