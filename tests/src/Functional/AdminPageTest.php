<?php

namespace Drupal\Tests\confirmation\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Simple browser test.
 *
 * @group confirmation
 */
class AdminPageTest extends BrowserTestBase {

  protected static $modules = [
    'confirmation_example',
  ];

  protected $defaultTheme = 'stark';

  public function testAdminPage() {
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('admin');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertTrue(TRUE);
  }

}
